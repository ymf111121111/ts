import { isArray } from "util";

// TODO 居然 args[0] 才是参数列表 , args[1] 为 undefined;
function createArrayTwoParam(...args: any[]) {
    return [args[0][0], args[0][1]].flat();
}

function createArrayThreeParam(...args: any[]) {
    let target = [args[0][0], args[0][1], args[0][2]].flat();

    return (copyCount: number) => {
        let newTarget: any[] = [];
        while (--copyCount >= 0) {
            newTarget = newTarget.concat(target);
        }
        return newTarget;
    };
}

export function createArray(...args: any[]): any {
    let paramLength = args.length;
    if (paramLength === 2) {
        return createArrayTwoParam(args);
    }

    if (paramLength === 3) {
        return createArrayThreeParam(args);
    }

    console.log("Param count unsupported");
    return undefined;
}

console.log(createArray([1, 2, 3], ["a", "b"]));
console.log(createArray(1, 2, 3)(3));
