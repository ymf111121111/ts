function convertNumber(value: number) {
    return value * 2;
}
function convertString(value: string) {
    return value.toUpperCase() + "!";
}

export function convert(value: string | number) {
    if (typeof value === "string") {
        return convertString(value);
    }

    if (typeof value === "number") {
        return convertNumber(value);
    }
}

console.log(convert(10));
console.log(convert("hello"));
