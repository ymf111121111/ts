export class TestCreateFuncByProtoType {}

let a = new TestCreateFuncByProtoType();

Object.defineProperty(a, "celebrateBirthday", { value: () => console.log("Create celebrateBirthday finish") });

(a as any).celebrateBirthday();
