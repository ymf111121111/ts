// 实现 Map 的 keys、values 方法

// TODO Map 大致思路为索引类型去实现,实现起来比较麻烦有时间在实现
// interface KMap<K extends number, V> {
//     [key: string]: V; //索引签名
// }
export function kKeys<K, V>(map: Map<K, V>) {
    return map.keys();
}

export function kValues<K, V>(map: Map<K, V>) {
    return map.values();
}
