// 实现数组的 forEach、concat、copyWithin、filter、map、shift、
// unshift、reduce、reverse、flat、findIndex、find、some、sort、slice、splice 等方法

import { isNode } from "yaml";

type MapCallBackType<T> = (value: T, index: number, array: T[]) => T;
type ForeachCallBackType<T> = (value: T, index: number) => void;
type FilterCallBackType<T> = (value: T, index: number, array: T[]) => boolean;
type ReduceCallBackType<T> = (preValue: T, currValue: T, currIndex: number, array: T[]) => T;
type FindCallBackType<T> = (value: T, index: number, obj: T[]) => boolean;
type SomeCallBackType<T> = (value: T, index: number, obj: T[]) => unknown;
type SortCallBackType<T> = (a: T, b: T) => number;

export function kForeach<T>(arr: T[], callback: ForeachCallBackType<T>) {
    for (let i = 0; i < arr.length; ++i) {
        callback(arr[i], i);
    }
}

export function kConcat<T>(arr: T[], targetList: T[]) {
    if (!Array.isArray(targetList)) {
        return [...arr, targetList];
    }
    return [...arr, ...targetList];
}

export function kCopyWithin<T>(arr: T[], target: number, start?: number, end?: number) {
    let length = arr.length;
    if (target >= length) {
        return arr;
    }

    let startIndex = start === undefined || target < 0 ? 0 : start;
    let endIndex = end ?? length;

    let tempList = [];
    for (let i = startIndex; i < Math.min(endIndex, length); ++i) {
        tempList.push(arr[i]);
    }

    for (let i = 0; i < tempList.length; ++i) {
        arr[target++] = tempList[i];
    }

    return arr;
}

export function kFilter<T>(arr: T[], callback: FilterCallBackType<T>) {
    let newList = [];
    for (let i = 0; i < arr.length; ++i) {
        let value = arr[i];
        if (callback(value, i, arr)) {
            newList.push(value);
        }
    }

    return newList;
}

export function kMap<T>(arr: T[], callback: MapCallBackType<T>) {
    let newList = [];
    for (let i = 0; i < arr.length; ++i) {
        newList.push(callback(arr[i], i, arr));
    }

    return newList;
}

export function kShift<T>(arr: T[]) {
    let value: T;
    [value, ...arr] = [...arr];
    return value;
}

export function kUnshift<T>(arr: T[], newValue: number) {
    return [newValue, ...arr];
}

export function kReduce<T>(arr: T[], callback: ReduceCallBackType<T>, initialValue: T) {
    let res: T = initialValue;
    for (let i = 0; i < arr.length; ++i) {
        res = callback(res, arr[i], i, arr);
    }

    return res;
}

export function kReverse<T>(arr: T[]) {
    let endIndex = arr.length - 1;

    for (let i = 0; i < arr.length / 2; ++i) {
        [arr[i], arr[endIndex - i]] = [arr[endIndex - i], arr[i]];
    }

    return arr;
}

export function KFlat(arr: any[], num = 1): any {
    return num > 0 ? kReduce(arr, (pre, cur) => kConcat(pre, Array.isArray(cur) ? KFlat(cur, num - 1) : cur), []) : arr;
}

export function kFindIndex<T>(arr: T[], callback: FindCallBackType<T>) {
    let findIndex = -1;
    kForeach(arr, (v, i) => {
        if (callback(v, i, arr)) {
            findIndex = i;
        }
    });

    return findIndex;
}

export function kFind<T>(arr: T[], callback: FindCallBackType<T>) {
    let findIndex = -1;
    kForeach(arr, (v, i) => {
        if (callback(v, i, arr)) {
            findIndex = i;
        }
    });

    return arr[findIndex];
}

export function kSome<T>(arr: T[], callback: SomeCallBackType<T>) {
    for (let i = 0; i < arr.length; ++i) {
        if (callback(arr[i], i, arr) !== undefined) {
            return;
        }
    }
}

// TODO 懒得写排序了
export function kSort<T>(arr: T[], callback: SortCallBackType<T>) {
    return arr.sort(callback);
}

export function kSlice<T>(arr: T[], start?: number, end?: number) {
    let length = arr.length;
    let startIndex = start ?? 0;
    let endIndex = end ?? length;

    let newList: T[] = [];
    if (startIndex >= length || endIndex > length) {
        return newList;
    }

    for (let i = startIndex; i < endIndex; ++i) {
        newList.push(arr[i]);
    }

    return newList;
}

export function kSplice<T>(arr: T[], start: number, count?: number, ...newValue: T[]) {
    let length = arr.length;
    let endIndex = count === undefined ? arr.length : Math.min(arr.length, start + count);

    let removeList = kSlice<T>(arr, start, endIndex);
    // 浅拷贝一下
    Object.assign(arr, [...kSlice<T>(arr, 0, start), ...newValue, ...kSlice<T>(arr, endIndex, length)]);
    return removeList;
}
