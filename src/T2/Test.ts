import { kConcat, kFind, kFindIndex, kShift, kSlice, kSome, kSplice } from "./KArray";

let array1 = [1, 2, 3, 4, [5, 2, [6, 7, 3, [1, 2, 8]]]];

let a = [1, 2, 3];

let b = kSplice(a, 1, 1, 100, 100, 100);
// let b = a.splice(1, 1, 100, 100, 100);
console.log(b);
console.log(a);

// function Test(a: number[]) {
//     a = [...a, ...[1, 2, 3]];
//     console.log(a);
// }

// Test(a);
// console.log(a);
