
export function catchError() {
    return function (target: any) {
        Object.getOwnPropertyNames(target.prototype).forEach((funcName) => {
            // 构造函数不需要 catch
            if (target.prototype[funcName] === target.prototype.constructor) {
                return;
            }

            // TODO Object.getOwnPropertyDescriptors(target)[funcName] 与 target.prototype[funcName] 相等, 但却重写不了
            // console.log(target.prototype);

            // TODO : 因为是对类的修改，所有的改动得在原型链上做修改，对 propertyDescriptors 修改则无效
            // 而方法装饰器可以是因为改的是方法本身(暂时这么理解上面的问题)

            const sourceFunc = target.prototype[funcName];
            target.prototype[funcName] = function (...args: any[]) {
                try {
                    return sourceFunc.call(this, args);
                } catch (error) {
                    console.error(`Error!!!!!!!!!!!!!!`);
                }
            };
        });
    };
}

@catchError()
class TestCatchError {
    public add(a: number, b: number) {
        throw error("1");
    }
}

let a = new TestCatchError();
a.add(1, 1);
