import * as fs from "fs";
import * as path from "path";

let readFilePath = path.normalize("D://Study//TS//generated//DefaultGameplayTags.ini");
let data = fs.readFileSync(readFilePath, { encoding: "utf8", flag: "r" });

let inputList = data.split("\r\n");

let out: string[] = [];

inputList.forEach((input) => {
    if (input.includes("+GameplayTagList=(Tag=\"")) {
        out.push(input.replace("+GameplayTagList=(Tag=\"","").split("\"").shift() ?? "");
    }
});

console.log(out);
