import { IConversionOptions, Root, loadSync } from "protobufjs";

// 定义 proto root
let bufRoot = new Root();

// 将 proto 文件加载进 root
loadSync(["proto/battle.proto", "proto/client.proto", "proto/code.proto"], bufRoot);

// 根据定义的数据结构将 ts 的数据结构转换为服务器认识的结构
let data = {
    battleType: 123,
};
let messageType = bufRoot.lookupType("genesis.battle.ResponseBattleBegin");
// 最终数据
let message = messageType.create(data);

// 根据类型打包为 二进制 发送服务器
let buffer = messageType.encode(message).finish();

// 将服务回传的数据解码
let response = messageType.decode(buffer);

// 根据定义好的数据转换选项将其解析为 ts 认识对象
const MESSAGE_COVERT_OPTIONS: IConversionOptions = {
    longs: Number,
    enums: Number,
    bytes: Uint8Array,
    oneofs: true,
    defaults: true,
};
let object = messageType.toObject(response, MESSAGE_COVERT_OPTIONS);
console.log(object);
