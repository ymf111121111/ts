import * as fs from "fs";

let a = [1, 2, 3, 4];
let b = [11, 22, 33, 44];

let c: number[] = [];

export function syncReadTxt(path: string) {
    return new Promise((resolve, rejects) => {
        fs.readFile(__dirname + path, { encoding: "utf8" }, (error, data) => {
            resolve(data);
            rejects(() => console.log(error));
        });
    });
}

// async

// async function fn() {
//     const res1 = await readTxt(a);
//     const res2 = await readTxt(res1);
//     console.log(res2);
// }

// promiseAll

let promise1 = syncReadTxt("/data/1.txt");

let promise2 = syncReadTxt("/data/2.txt");

Promise.all([promise1, promise2]).then((value) => {
    // 写入文件
    let data = "";
    value.forEach((v) => (data = data + v));
    fs.writeFile(__dirname + "/data/target.txt", data, (error) => console.log(error));
});
