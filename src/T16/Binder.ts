import { trace } from "console";

// TODO 可能使用装饰器也可以实现
export function decoratorBinder(target: any, property: string, descriptor: PropertyDescriptor) {
    // Object.defineProperty(target, property, {
    //     get: function () {
    //         console.log(111111111111111111);
    //         return target[property].bind(this);
    //     },
    //     set: function (v) {
    //         console.log(111111111111111111);
    //     },
    // });

    descriptor.value = target[property].bind(target);

    // console.log(Object.prototype.);
}

// 函数版本
export function binder(func: Function, target: Object) {
    return func.bind(target);
}

class Animal {
    name: string;

    constructor(name: string) {
        this.name = name;
    }

    speak() {
        console.log(`Hello, my name is ${this.name}`);
    }
}

class Dog extends Animal {
    // @decoratorBinder
    speak() {
        console.log(`Hello, my name is ${this.name} and I'm a dog`);
    }
}

// 将 dog 中 speak 赋值给了函数，但是该函数中没有 name 这个属性

const dog = new Dog("Fido");
const speak = dog.speak;
// 此时 speak 的 this 为指向全局 window 的 this
speak();
