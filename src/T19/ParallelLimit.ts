import { chunk } from "lodash";

async function parallelLimit(fnArray: Function[], limit: number, timeout: number) {
    let promiseList: Promise<number>[] = [];
    let timeoutPromise = new Promise((resolve, reject) => {
        setTimeout(() => {
            reject("timeout");
        }, timeout);
    });

    fnArray.forEach((func) => {
        let promise = func();
        promiseList.push(Promise.race([promise, timeoutPromise]));
    });
    let res: number[] = [];

    let limitPromiseList = chunk(promiseList, limit);

    try {
        for (const promiseList of limitPromiseList) {
            res = res.concat(await Promise.all(promiseList));
        }
        return res;
    } catch (error) {
        console.log(error);
        return;
    }
}

async function mockAsyncTask(i: number): Promise<number> {
    const time = Math.random() * 1000 + 1000;
    return new Promise((resolve) => {
        setTimeout(() => {
            resolve(i);
        }, time);
    });
}

async function test() {
    const fnArray = [1, 2, 6, 4, 5].map((i) => () => mockAsyncTask(i));
    const result = await parallelLimit(fnArray, 2, 1000);
    console.log(result); // [3, 1, 2, 4, 5] 不知道什么顺序
}

test();
