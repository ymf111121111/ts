// 实现一个类型过滤器类型 Filter<T, U>，用于过滤类型 T 中所有与类型 U 匹配的元素，并将这些元素组合成一个新的元素数组类型。

// 遍历 T 的所有类型 ,keyof 可以拿到 T 中所有类型名字再用 T[P] 取出其类型
export type TestKeyof<T> = {
    [P in keyof T]: T[P];
};

// ES5 中实现的：如果 T 为 U 的子集则返回 T 否则 never
export type Extract<T, U> = T extends U ? T : never;

export type Filter<T, U> = {
    [P in keyof T]: Extract<T[P], U>;
};

type Fruit = "apple" | "banana" | "orange";
type Fruits = ["apple", "banana", "orange", "cherry"];

type OnlyFruits = Filter<Fruits, Fruit>; // ['apple', 'banana', 'orange']

type TestKeyofInstance = TestKeyof<Fruit>;
