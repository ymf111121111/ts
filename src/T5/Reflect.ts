export function hasWithTypes(target: any, propertyKey: string, propertyType: string) {
    return Reflect.has(target, propertyKey) && typeof Reflect.get(target, propertyKey) === propertyType;
}

class TestA {
    public name: string = "1";
}

let a = new TestA();

console.log(hasWithTypes(a, "name", "number"));
