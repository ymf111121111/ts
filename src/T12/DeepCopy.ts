const cacheMap = new Map();
export function deepClone(target: any) {
    // 基础类型直接返回，不需要深拷贝
    if (typeof target !== "object") {
        return target;
    }

    const cache = cacheMap.get(target);
    if (cache) {
        return cache;
    }

    // 特判 Map
    if (target instanceof Map) {
        let res = new Map();
        target.forEach((v, k) => res.set(k, deepClone(v)));
        return res;
    }

    // 特判 Set
    if (target instanceof Set) {
        let res = new Set();
        target.forEach((v) => res.add(deepClone(v)));
        return res;
    }

    // obj7: any = {
    //     a: 1 ,
    //     b: obj7
    // };
    let deepObject = Array.isArray(target) ? [] : {};
    // 这里存的是浅拷贝
    cacheMap.set(target, deepObject);

    // 如果为数组 in 则遍历的 index，如果为 Object 则遍历所有属性
    for (let key in target) {
        if (target.hasOwnProperty(key)) {
            (deepObject as any)[key] = deepClone(target[key]);
        }
    }

    return deepObject;
}

const targetArray = [1, 2, 34];

// console.log(cloneDeep(targetArray));

// let a = {
//     name: "s",
//     age: 12,
// };

// for (let key in targetArray) {
//     console.log(key);
// }

// obj7: any = {
//     a: 1 ,
//     b: obj7
// };

const obj7: any = { a: 1 };
obj7.b = obj7;
const cloned7 = deepClone(obj7);
console.log(cloned7.a); // 1
console.log(cloned7.b === cloned7);
