/** 消息队列 */

function func1() {
    console.log(1);
}

function func2() {
    setTimeout(() => {
        console.log(2);
    }, 1000);
}

func2();
func1();

// /** 微任务 消息队列 */
// let p = new Promise((resolve) => {
//     console.log(1);
//     resolve(5);
// });

// function func2() {
//     setTimeout(() => {
//         console.log(2);
//     }, 1);

//     p.then((resolve) => {
//         console.log(resolve);
//     }).then(() => {
//         console.log(6);
//     });
// }

// func2();
