/**
 * @param resolve 异步操作成功时的回调
 * @param reject 异步操作失败时的回调
 */
let p = new Promise((resolve, reject) => {
    console.log(1);
    reject(5);
});

p.then(
    (res) => {
        console.log(6);
        return new Promise((resolve, reject) => {
            resolve(100);
        });
    },
    (err) => {
        console.log(err);
    },
)
    .then((res) => {
        console.log(res);
    })
    .catch((err) => {
        console.log("出现异常");
    });
