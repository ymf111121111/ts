// 该函数等待 fetch 的服务器返回
async function TestAsync1() {
    let response = await fetch("123");
    return response;
}

async function TestAsync2() {
    let response1 = await fetch("123");
    let response2 = await fetch("123");
}

// 优化：效率比上一个函数快了一倍
async function TestAsync3() {
    let promise1 = fetch("123");
    let promise2 = fetch("123");

    let [response1, response2] = await Promise.all([promise1, promise2]);
}
