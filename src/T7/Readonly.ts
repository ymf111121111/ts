interface A {
    name: string;
    age: string;
    desc: string;
}

//就是`keyof T` 拿到 `T` 所有属性名, 然后 `in` 进行遍历, 将值赋给 `K`, 最后 `T[K]` 取得相应属性的值时将其设置为readonly。
export type DeepReadonly<T> = {
    readonly [K in keyof T]: T[K];
};

let a: DeepReadonly<A> = {
    name: "1",
    age: "2",
    desc: "3",
};

// a.name = "1";
