// 请实现一个函数 createDeepProxy，可以对一个对象进行深度监听，并在对象的任意属性被读取或设置时触发回调函数。具体要求如下：
// 使用 Proxy 对象来实现深度监听对象。
// 回调函数的参数包括：被读取/设置的属性名、属性的旧值和新值。
// 要求监听对象的子对象也能够触发回调函数。


// 重写 Get Set 即可

// TODO 只监听了第一层，后期递归重写所有 set get
export type DeepProxyCallBackType = (property: string, oldValue: any, newValue: any) => void;

export function createDeepProxy(target: any, callBack: DeepProxyCallBackType, key: string = "") {
    for (let property in target) {
        // 需要把值赋值一下，不然会循环引用导致爆栈
        let oldValue = target[property];
        if (typeof oldValue === "object") {
            createDeepProxy(oldValue, callBack, property);
            continue;
        }

        let propertyDesc = Object.getOwnPropertyDescriptor(target, property);
        if (property === undefined) continue;

        const propertyKey = (key === "" ? "" : `${key}.`) + property;
        propertyDesc = {
            get: function () {
                callBack(propertyKey, oldValue, oldValue);
                //TODO 此写法爆栈了 return target[property]
                return oldValue;
            },
            set: function (newValue) {
                callBack(propertyKey, oldValue, newValue);
                oldValue = newValue;
            },
        };

        Object.defineProperty(target, property, propertyDesc);
    }

    return target;
}

interface Test {
    name: string;
    age: number;
    info: Object;
}

// 测试代码
const obj: Test = {
    name: "Tom",
    age: 18,
    info: {
        address: "Beijing",
    },
};

/** 手动实现 Proxy */

// const proxy = createDeepProxy(obj, (prop, oldValue, newValue) => {
//     console.log(`属性 ${prop} 从 ${oldValue} 变成了 ${newValue}`);
// });

// // // console.log(Object.getOwnPropertyDescriptors(proxy));

// // proxy.name = "Jerry"; // 属性 name 从 Tom 变成了 Jerry
// proxy.name = "111121111";
// // console.log(proxy.name);
// // proxy.age = 19; // 属性 age 从 18 变成了 19
// proxy.info.address = "Shanghai"; // 属性 info.address 从 Beijing 变成了 Shanghai

/** 使用 Proxy 库 */

let proxyObj = new Proxy<Test>(obj, {
    get(target: any, propertyKey: string, receiver: any) {
        console.log(`属性 ${propertyKey} 值为 ${target[propertyKey]} `);
    },
    set(target: any, propertyKey: string, newValue: any, receiver: any) {
        console.log(`属性 ${propertyKey} 从 ${target[propertyKey]} 变成了 ${newValue}`);
        target[propertyKey] = newValue;
        return true;
    },
});

proxyObj.age = 20;
proxyObj.age = 22;